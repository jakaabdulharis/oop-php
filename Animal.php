<?php

class Animal
{
    public $name;
    public $legs = 2;
    public $cold_blooded = "false";
    // public function __construct($name){
    //     $this->name = $name;
    // }

    // Coba Pakai Methods get_name
    function set_name($name) {
        $this->name = $name;
    }
    function get_name() {
        return $this->name;
    }    
}

?>