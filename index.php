<?php
require_once('Animal.php');
require_once("Ape.php");
require_once("Frog.php");

// $sheep = new Animal("shaun");
echo "----------------------------------<br>";
echo "Release 0<br>";
echo "----------------------------------<br>";
$sheep = new Animal();
$sheep->set_name('shaun');
echo "Animal Name : $sheep->name <br>"; // "shaun"
echo "Legs : $sheep->legs <br>"; // 2
echo "Cold Blooded : $sheep->cold_blooded <br>"; // false


echo "----------------------------------<br>";
echo "Release 1<br>";
echo "----------------------------------<br>";
echo "<b>Class Ape</b><br>";
$sungokong = new Ape("kera sakti");
echo "Yell : "; $sungokong->yell(); // "Auooo"
echo "<br>";
echo "----------------------------------<br>";



echo "<b>Class Frog</b><br>";
$kodok = new Frog("buduk");
echo "Legs : $kodok->legs <br>"; // 2
echo "Jump : ";$kodok->jump() ; // "hop hop"
?>